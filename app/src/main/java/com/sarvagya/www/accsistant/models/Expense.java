package com.sarvagya.www.accsistant.models;

/**
 * Created by User on 10/20/2017.
 */

public class Expense {

    private int expense_id;
    private double expenseAmt;
    private String expenseDetails;
    private long date;
    private String moreDetails;

    public Expense() {

    }

    public Expense(double expenseAmt, String expenseDetails, long dateTime, String moreDetails) {
        this.expenseAmt = expenseAmt;
        this.expenseDetails = expenseDetails;
        this.date = dateTime;
        this.moreDetails = moreDetails;
    }

    public int getExpense_id() {
        return expense_id;
    }

    public void setExpense_id(int expense_id) {
        this.expense_id = expense_id;
    }

    public double getExpenseAmt() {
        return expenseAmt;
    }

    public void setExpenseAmt(double expenseAmt) {
        this.expenseAmt = expenseAmt;
    }

    public String getExpenseDetails() {
        return expenseDetails;
    }

    public void setExpenseDetails(String expenseDetails) {
        this.expenseDetails = expenseDetails;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getMoreDetails() {
        return moreDetails;
    }

    public void setMoreDetails(String moreDetails) {
        this.moreDetails = moreDetails;
    }


}
