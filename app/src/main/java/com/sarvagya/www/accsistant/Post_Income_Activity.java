package com.sarvagya.www.accsistant;

import android.annotation.SuppressLint;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sarvagya.www.accsistant.db.DatabaseHandler;
import com.sarvagya.www.accsistant.models.Income;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Post_Income_Activity extends AppCompatActivity {

    private RelativeLayout relativeLayout;
    private TextView tv_postIncomeText, tv_incomeAmt, tv_incomeDetails, tv_date;
    private EditText et_incomeAmt;
    private EditText et_incomeDetails, et_moreDetails;
    private Button btn_post;
    private DatabaseHandler handler;
    private TextInputLayout til, til2;

    private TextView tv_third, tv_fifth, tv_postedDate;
    private long dateTime;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post__income_);

        init();

        handler = new DatabaseHandler(this);

        Calendar cal = Calendar.getInstance();
        dateTime = cal.getTimeInMillis();

        Date date = new Date(dateTime);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = sdf.format(date);

        tv_date.setText(formattedDate);

        lastTransaction();

        btn_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                til.setErrorEnabled(false);
                til2.setErrorEnabled(false);

                if (et_incomeAmt.getText().length() == 0) {
                    til.setError("amount required");
                } else if (et_incomeDetails.getText().length() == 0) {
                    til2.setError("particulars required");
                } else {
                    til.setErrorEnabled(false);
                    til2.setErrorEnabled(false);
                    saveTransaction();
                }
            }
        });


    }

    public void saveTransaction() {
        double incomeAmt = Double.parseDouble(et_incomeAmt.getText().toString());
        String incomeDetails = et_incomeDetails.getText().toString();
        String moreDetails = et_moreDetails.getText().toString();

        Income income = new Income(incomeAmt, incomeDetails, dateTime, moreDetails);
        long a = handler.addIncome(income);

        if (a != -1) {

            Snackbar bar = Snackbar.make(relativeLayout, "Income/Receipt entry posted!", Snackbar.LENGTH_LONG);
            bar.show();

            et_incomeAmt.setText("");
            et_incomeDetails.setText("");
            et_moreDetails.setText("");

        } else {

            Snackbar bar = Snackbar.make(relativeLayout, "Income/Receipt entry couldn't be posted!", Snackbar.LENGTH_LONG);
            bar.show();
        }

        lastTransaction();
    }

    public void lastTransaction() {
        Income income1 = new Income();
        income1 = handler.getLastIncome();

        double amt = income1.getIncomeAmt();
        String details = income1.getIncomeDetails();
        long dateValue = income1.getDateTime();

        Date date = new Date(dateValue);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String dateFormatted = sdf.format(date);

        tv_third.setText("Rs. " + String.valueOf(amt));
        tv_fifth.setText(details);
        tv_postedDate.setText("Posted on date : " + dateFormatted);
    }


    public void init(){

        relativeLayout =(RelativeLayout) findViewById(R.id.relativeLayout);

        til = (TextInputLayout) findViewById(R.id.til);
        til2 = (TextInputLayout) findViewById(R.id.til2);

        tv_third = (TextView) findViewById(R.id.tv_third);
        tv_fifth = (TextView) findViewById(R.id.tv_fifth);
        tv_postedDate = (TextView) findViewById(R.id.tv_postedDate);

        et_incomeAmt = (EditText) findViewById(R.id.et_incomeAmt);

        et_incomeDetails = (EditText) findViewById(R.id.et_incomeDetails);
        et_moreDetails = (EditText) findViewById(R.id.et_moreDetails);
        btn_post = (Button) findViewById(R.id.btn_post);

        tv_date = (TextView) findViewById(R.id.tv_date);
    }
}
