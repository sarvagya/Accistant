package com.sarvagya.www.accsistant.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aspose.cells.CellsHelper;
import com.aspose.cells.Workbook;
import com.sarvagya.www.accsistant.R;
import com.sarvagya.www.accsistant.controller.IncomeAdapter;
import com.sarvagya.www.accsistant.db.DatabaseHandler;
import com.sarvagya.www.accsistant.models.Income;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;



/**
 * A simple {@link Fragment} subclass.
 */
public class fragment_income_view extends Fragment {
    private RecyclerView recyclerView;
    private DatabaseHandler handler;
    private ArrayList<Income> incomeList_main;
    private boolean isClicked = false;
    private IncomeAdapter adapter;


    private TextView tv_totalIncome, tv_highestValue, tv_lowestValue, tv_averageValue, tvNothingText;


    View view;

    public fragment_income_view() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_fragment_income_view, container, false);
        tv_totalIncome = view.findViewById(R.id.tv_totalIncome);
        tv_highestValue = view.findViewById(R.id.tv_highestValue);
        tv_lowestValue = view.findViewById(R.id.tv_lowestValue);
        tv_averageValue = view.findViewById(R.id.tv_averageValue);
        tvNothingText = view.findViewById(R.id.tvNothingText2);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (view != null) {

            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerView.setHasFixedSize(false);

            handler = new DatabaseHandler(getActivity());
            incomeList_main = handler.getAllIncome();

            if (incomeList_main.size() >0){
                adapter = new IncomeAdapter(getActivity(), incomeList_main);
                recyclerView.setAdapter(adapter);
            }else {
                tvNothingText.setVisibility(View.VISIBLE);
            }


            double total = handler.getTotalIncome();
            tv_totalIncome.setText("TOTAL INCOME : " + total);

            tv_highestValue.setText(handler.getHighestValue(1) + "");
            tv_lowestValue.setText(handler.getLowestValue(1) + "");
            tv_averageValue.setText(new DecimalFormat("##.##").format(handler.getAverageValue(1)));
        }
    }
}


