package com.sarvagya.www.accsistant;

import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sarvagya.www.accsistant.db.DatabaseHandler;
import com.sarvagya.www.accsistant.models.Expense;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Post_Expense_Activity extends AppCompatActivity {
    private TextView tv_date;
    private EditText et_expenseAmt, et_expenseDetails, et_moreDetails;
    private Button btn_post;
    private DatabaseHandler handler;

    private TextView tv_third, tv_fifth, tv_postedDate;
    private TextInputLayout til, til2, til3;

    private long dateTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post__expense_);

        init();

        handler = new DatabaseHandler(this);

        Calendar cal = Calendar.getInstance();
        dateTime = cal.getTimeInMillis();

        Date date = new Date(dateTime);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = sdf.format(date);
        tv_date.setText(formattedDate);

        getLastExpense();

        btn_post = (Button) findViewById(R.id.btn_post);
        btn_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                til.setErrorEnabled(false);
                til2.setErrorEnabled(false);

                if(et_expenseAmt.getText().length() == 0){
                    til.setError("Amount required !");
                }else if (et_expenseDetails.getText().length() == 0){
                    til2.setError("Particulars required !");
                }else {
                    til.setErrorEnabled(false);
                    til2.setErrorEnabled(false);
                    saveTransaction();
                }
            }
        });
    }

    public void saveTransaction() {
        double expenseAmt = Double.parseDouble(et_expenseAmt.getText().toString());
        String expenseDetails = et_expenseDetails.getText().toString();
        String moreDetails = et_moreDetails.getText().toString();

        Expense expense = new Expense(expenseAmt, expenseDetails, dateTime, moreDetails);
        long a = handler.addExpense(expense);

        if (a != -1) {

            Snackbar bar = Snackbar.make(btn_post, "Expense/Payment entry posted!", Snackbar.LENGTH_LONG);
            bar.show();
            et_expenseAmt.setText("");
            et_expenseDetails.setText("");
            et_moreDetails.setText("");

        } else {

            Snackbar bar = Snackbar.make(btn_post, "Expense/Payment entry couldn't be posted!", Snackbar.LENGTH_LONG);
            bar.show();
        }
        getLastExpense();
    }

    public void getLastExpense() {
        Expense expense1 = new Expense();
        expense1 = handler.getLastExpense();

        double amt = expense1.getExpenseAmt();
        String details = expense1.getExpenseDetails();
        long dateValue = expense1.getDate();

        Date date = new Date(dateValue);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = sdf.format(date);

        tv_third.setText("Rs. " + String.valueOf(amt));
        tv_fifth.setText(details);
        tv_postedDate.setText("Posted on date : " + formattedDate);

    }

    public void init(){
        tv_third = (TextView) findViewById(R.id.tv_third);
        tv_fifth = (TextView) findViewById(R.id.tv_fifth);
        tv_postedDate = (TextView) findViewById(R.id.tv_postedDate);

        tv_date = (TextView) findViewById(R.id.tv_date);
        et_expenseAmt = (EditText) findViewById(R.id.et_expenseAmt);
        et_expenseDetails = (EditText) findViewById(R.id.et_expenseDetails);
        et_moreDetails = (EditText) findViewById(R.id.et_moreDetails);

        til = (TextInputLayout) findViewById(R.id.til);
        til2 = (TextInputLayout) findViewById(R.id.til2);
        til3 = (TextInputLayout) findViewById(R.id.til3);
    }
}
