package com.sarvagya.www.accsistant.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.sarvagya.www.accsistant.models.Expense;
import com.sarvagya.www.accsistant.models.Income;

import java.util.ArrayList;

/**
 * Created by User on 10/16/2017.
 */

public class DatabaseHandler extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "income";
    public static final int DATABASE_VERSION = 1;

    private static final String TABLE_INCOME = "income";

    private static final String KEY_ID_income = "id";
    private static final String KEY_income_amt = "income_amt";
    private static final String KEY_income_details = "income_details";
    private static final String KEY_date_time = "date_time";
    private static final String KEY_details = "details";

    private static final String TABLE_EXPENSE = "expense";

    private static final String KEY_ID_expense = "id";
    private static final String KEY_expense_amt = "expense_amt";
    private static final String KEY_expense_details = "expense_details";
    private static final String KEY_date_time_expense = "date_time";
    private static final String KEY_details_expense = "details";


    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createIncomeTable = "CREATE TABLE " + TABLE_INCOME + "(" + KEY_ID_income + " INTEGER PRIMARY KEY," + KEY_income_amt + " DOUBLE," +
                KEY_income_details + " TEXT," + KEY_date_time + " LONG, " + KEY_details + " TEXT" + ")";

        String createExpenseTable = "CREATE TABLE " + TABLE_EXPENSE + "(" + KEY_ID_expense + " INTEGER PRIMARY KEY," + KEY_expense_amt + " DOUBLE," +
                KEY_expense_details + " TEXT," + KEY_date_time_expense + " LONG," + KEY_details_expense + " TEXT" + ")";

        db.execSQL(createIncomeTable);
        db.execSQL(createExpenseTable);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("Drop table if exists " + TABLE_INCOME);
        db.execSQL("Drop table if exists " + TABLE_EXPENSE);

        onCreate(db);
    }

    public long addIncome(Income income) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_income_amt, income.getIncomeAmt());
        values.put(KEY_income_details, income.getIncomeDetails());
        values.put(KEY_date_time, income.getDateTime());
        values.put(KEY_details, income.getMoreDetails());

        long r = db.insert(TABLE_INCOME, null, values);
        db.close();

        return r;
    }

    public ArrayList<Income> getAllIncome() {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<Income> incomeList = new ArrayList<>();

        String query = "Select * from " + TABLE_INCOME;

        Cursor cursor = db.rawQuery(query, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    Income income = new Income();
                    income.setIncome_id(cursor.getInt(cursor.getColumnIndex(KEY_ID_income)));
                    income.setIncomeAmt(cursor.getDouble(cursor.getColumnIndex(KEY_income_amt)));
                    income.setIncomeDetails(cursor.getString(cursor.getColumnIndex(KEY_income_details)));
                    income.setDateTime(cursor.getLong(cursor.getColumnIndex(KEY_date_time)));
                    income.setMoreDetails(cursor.getString(cursor.getColumnIndex(KEY_details)));

                    incomeList.add(income);
                } while (cursor.moveToNext());
            }
        }

        cursor.close();
        db.close();
        return incomeList;
    }

    public ArrayList<Income> getSortedIncome(long date_from, long date_to){
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<Income> incomeList = new ArrayList<>();

        String query = "Select * from " + TABLE_INCOME + " Where " + KEY_date_time + " between " + date_from + " and " + date_to;

        Cursor cursor = db.rawQuery(query, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    Income income = new Income();
                    income.setIncome_id(cursor.getInt(cursor.getColumnIndex(KEY_ID_income)));
                    income.setIncomeAmt(cursor.getDouble(cursor.getColumnIndex(KEY_income_amt)));
                    income.setIncomeDetails(cursor.getString(cursor.getColumnIndex(KEY_income_details)));
                    income.setDateTime(cursor.getLong(cursor.getColumnIndex(KEY_date_time)));
                    income.setMoreDetails(cursor.getString(cursor.getColumnIndex(KEY_details)));

                    incomeList.add(income);
                } while (cursor.moveToNext());
            }
        }

        cursor.close();
        db.close();
        return incomeList;
    }

    public ArrayList<Expense> getSortedExpense(long date_from, long date_to){
        ArrayList<Expense> expenseList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        String query = "Select * from " + TABLE_EXPENSE + " Where " + KEY_date_time_expense + " between " + date_from + " and " + date_to;

        Cursor cursor = db.rawQuery(query, null);
        if(cursor !=null){
            if(cursor.moveToFirst()){
                do{
                    Expense expense = new Expense();
                    expense.setExpense_id(cursor.getInt(cursor.getColumnIndex(KEY_ID_expense)));
                    expense.setExpenseAmt(cursor.getDouble(cursor.getColumnIndex(KEY_expense_amt)));
                    expense.setExpenseDetails(cursor.getString(cursor.getColumnIndex(KEY_expense_details)));
                    expense.setDate(cursor.getLong(cursor.getColumnIndex(KEY_date_time_expense)));
                    expense.setMoreDetails(cursor.getString(cursor.getColumnIndex(KEY_details_expense)));

                    expenseList.add(expense);
                }while (cursor.moveToNext());
            }
        }

        cursor.close();
        db.close();
        return expenseList;
    }

    public void deleteIncomeObject(int object_id){
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_INCOME, KEY_ID_income + "= " + object_id, null);
        db.close();
    }

    public void deleteExpenseObject(int object_id){
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_EXPENSE, KEY_ID_expense + "= " + object_id, null);
        db.close();
    }

    public Income getLastIncome(){
        SQLiteDatabase db = getReadableDatabase();
        Income income = new Income();

        String query = "Select * from " + TABLE_INCOME + " ORDER BY " + KEY_ID_income + " DESC limit 1";
        Cursor cursor = db.rawQuery(query, null);
        if(cursor.moveToLast()){

            income.setIncomeAmt(cursor.getDouble(cursor.getColumnIndex(KEY_income_amt)));
            income.setIncomeDetails(cursor.getString(cursor.getColumnIndex(KEY_income_details)));
            income.setDateTime(cursor.getLong(cursor.getColumnIndex(KEY_date_time)));

        }
        cursor.close();
        db.close();

        return income;
    }

    public Expense getLastExpense(){
        SQLiteDatabase db = getReadableDatabase();
        Expense expense = new Expense();

        String query = "Select * from " + TABLE_EXPENSE + " ORDER BY " + KEY_ID_expense + " DESC limit 1";
        Cursor cursor = db.rawQuery(query, null);
        if(cursor.moveToLast()){
            expense.setExpenseAmt(cursor.getDouble(cursor.getColumnIndex(KEY_expense_amt)));
            expense.setExpenseDetails(cursor.getString(cursor.getColumnIndex(KEY_expense_details)));
            expense.setDate(cursor.getLong(cursor.getColumnIndex(KEY_date_time_expense)));
        }

        cursor.close();
        db.close();

        return expense;
    }


    public double getTotalIncome(){

        double total = 0;

        SQLiteDatabase db = getReadableDatabase();

        String query = "Select SUM(" + KEY_income_amt + ") from " + TABLE_INCOME;
        Cursor cursor = db.rawQuery(query, null);
        if(cursor.moveToFirst()){
            total = cursor.getDouble(0);
        }

        return total;
    }









    public long addExpense(Expense expense) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_expense_amt, expense.getExpenseAmt());
        values.put(KEY_expense_details, expense.getExpenseDetails());
        values.put(KEY_details_expense, expense.getMoreDetails());
        values.put(KEY_date_time_expense, expense.getDate());

        long r = db.insert(TABLE_EXPENSE, null, values);
        db.close();

        return r;
    }

    public ArrayList<Expense> getAllExpense() {
        ArrayList<Expense> expenseList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        String query = "Select * from " + TABLE_EXPENSE;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    Expense expense = new Expense();
                    expense.setExpense_id(cursor.getInt(cursor.getColumnIndex(KEY_ID_expense)));
                    expense.setExpenseAmt(cursor.getDouble(cursor.getColumnIndex(KEY_expense_amt)));
                    expense.setExpenseDetails(cursor.getString(cursor.getColumnIndex(KEY_expense_details)));
                    expense.setDate(cursor.getLong(cursor.getColumnIndex(KEY_date_time_expense)));
                    expense.setMoreDetails(cursor.getString(cursor.getColumnIndex(KEY_details_expense)));

                    expenseList.add(expense);
                } while (cursor.moveToNext());
            }
        }
        cursor.close();
        db.close();

        return expenseList;
    }

    public double getTotalExpense(){
        SQLiteDatabase db = getReadableDatabase();
        double total = 0;

        String query = "Select SUM(" + KEY_expense_amt + ") from " + TABLE_EXPENSE;
        Cursor cursor = db.rawQuery(query, null);

        if(cursor!=null){
            if(cursor.moveToFirst()){
                total = cursor.getDouble(0);
            }
        }

        return total;
    }

    public void delete_table_income(){
        SQLiteDatabase db = getWritableDatabase();

        db.delete(TABLE_INCOME,null,null);
        db.close();

    }

    public void delete_table_expense(){
        SQLiteDatabase db = getWritableDatabase();

        db.delete(TABLE_EXPENSE,null,null);
        db.close();

    }

    public double getHighestValue(int key){
        double highestValue = 0;
        String query = null;
        SQLiteDatabase db = getReadableDatabase();

        if (key == 1){
            query = "Select" + " MAX (" + KEY_income_amt + ") from " + TABLE_INCOME;
        }else if (key == 2){
            query = "Select" + " MAX (" + KEY_expense_amt + ") from " + TABLE_EXPENSE;
        }
        Cursor cursor = db.rawQuery(query, null);

        if (cursor != null){
            if (cursor.moveToFirst()){
                highestValue = cursor.getDouble(0);
            }
        }
        return  highestValue;
    }

    public double getLowestValue(int key){
        double lowestValue = 0;
        String query = null;
        SQLiteDatabase db = getReadableDatabase();

        if (key == 1){
            query = "Select" + " MIN (" + KEY_income_amt + ") from " + TABLE_INCOME;
        }else if (key == 2){
            query = "Select" + " MIN (" + KEY_expense_amt + ") from " + TABLE_EXPENSE;
        }
        Cursor cursor = db.rawQuery(query, null);

        if (cursor != null){
            if (cursor.moveToFirst()){
                lowestValue = cursor.getDouble(0);
            }
        }
        return lowestValue;
    }

    public double getAverageValue(int key){
        double averageValue = 0;
        String query = null;
        SQLiteDatabase db = getReadableDatabase();

        if (key == 1){
            query = "Select" + " AVG (" + KEY_income_amt + ") from " + TABLE_INCOME;
        }else if (key == 2){
            query = "Select" + " AVG (" + KEY_expense_amt + ") from " + TABLE_EXPENSE;
        }
        Cursor cursor = db.rawQuery(query, null);

        if (cursor != null){
            if (cursor.moveToFirst()){
                averageValue = cursor.getDouble(0);
            }
        }
        return averageValue;
    }


}
