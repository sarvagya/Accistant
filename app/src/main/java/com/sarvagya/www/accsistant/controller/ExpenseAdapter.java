package com.sarvagya.www.accsistant.controller;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sarvagya.www.accsistant.R;
import com.sarvagya.www.accsistant.db.DatabaseHandler;
import com.sarvagya.www.accsistant.models.Expense;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class ExpenseAdapter extends RecyclerView.Adapter<ExpenseAdapter.CustomViewHolder>{

    private ArrayList<Expense> expenseList;
    private Context context;
    private LayoutInflater inflater;

    public ExpenseAdapter(ArrayList<Expense> list, Context context){
        this.context = context;
        this.expenseList = list;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.list_expense_items, parent , false);
        CustomViewHolder holder = new CustomViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        Expense expense1 = expenseList.get(position);

        holder.tv_expenseDetails.setText(expense1.getExpenseDetails());
        holder.tv_expenseAmt.setText(String.valueOf(expense1.getExpenseAmt()));
        holder.tv_moreDetails.setText(expense1.getMoreDetails());

        long dateTime = expense1.getDate();
        Date date = new Date(dateTime);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = sdf.format(date);

        holder.tv_dateTime.setText(formattedDate);

    }

    @Override
    public int getItemCount() {
        return expenseList.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_expenseDetails, tv_expenseAmt, tv_dateTime, tv_moreDetails;

        public CustomViewHolder(View itemView) {
            super(itemView);

            tv_expenseAmt = itemView.findViewById(R.id.tv_expenseAmt);
            tv_expenseDetails = itemView.findViewById(R.id.tv_expenseDetails);
            tv_moreDetails = itemView.findViewById(R.id.tv_moreDetails);
            tv_dateTime = itemView.findViewById(R.id.tv_dateTime);

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("Delete ?");
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            int position = getAdapterPosition();

                            Expense expense_object = expenseList.get(position);
                            int object_id = expense_object.getExpense_id();

                            DatabaseHandler handler = new DatabaseHandler(context);
                            handler.deleteExpenseObject(object_id);

                            expenseList.remove(position);
                            notifyItemRemoved(position);
                        }
                    });

                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();

                    return true;
                }
            });


        }
    }
}
