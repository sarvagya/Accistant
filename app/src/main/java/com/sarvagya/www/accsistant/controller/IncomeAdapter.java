package com.sarvagya.www.accsistant.controller;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sarvagya.www.accsistant.R;
import com.sarvagya.www.accsistant.db.DatabaseHandler;
import com.sarvagya.www.accsistant.models.Income;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class IncomeAdapter extends RecyclerView.Adapter<IncomeAdapter.CustomViewHolder>{

    private Context context;
    private ArrayList<Income> incomeList;
    private LayoutInflater inflater;

    public IncomeAdapter(Context context, ArrayList<Income> incomeList){
        this.incomeList = incomeList;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.list_income_items, parent, false);
        CustomViewHolder holder = new CustomViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        Income income1 = incomeList.get(position);

        holder.tv_incomeAmt.setText("Rs. " + income1.getIncomeAmt());
        holder.tv_incomeDetails.setText(income1.getIncomeDetails());
        holder.tv_moreDetails.setText(income1.getMoreDetails());

        long dateTime = income1.getDateTime();
        Date date = new Date(dateTime);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = sdf.format(date);

        holder.tv_dateTime.setText(formattedDate);
    }

    @Override
    public int getItemCount() {
        return incomeList.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_incomeAmt, tv_incomeDetails, tv_dateTime, tv_moreDetails;

        public CustomViewHolder(final View itemView) {
            super(itemView);

            tv_incomeAmt = itemView.findViewById(R.id.tv_incomeAmt);
            tv_incomeDetails = itemView.findViewById(R.id.tv_incomeDetails);
            tv_dateTime = itemView.findViewById(R.id.tv_dateTime);
            tv_moreDetails = itemView.findViewById(R.id.tv_moreDetails);

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {


                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("Delete ?");

                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            int position = getAdapterPosition();

                            Income income_object = incomeList.get(position);
                            int object_id = income_object.getIncome_id();
                            Log.d("legend", " " + object_id);

                            DatabaseHandler handler = new DatabaseHandler(context);
                            handler.deleteIncomeObject(object_id);

                            incomeList.remove(position);
                            notifyItemRemoved(position);
                        }
                    });

                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();

                    return true;
                }
            });

        }
    }
}
