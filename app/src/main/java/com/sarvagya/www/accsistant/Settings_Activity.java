package com.sarvagya.www.accsistant;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.transition.Explode;
import android.view.MenuItem;
import android.view.View;

import com.sarvagya.www.accsistant.fragments.DeleteDialogFragment;

public class Settings_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Settings");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    public void change_name(View view){
        Intent intent = new Intent(this, Change_Name_Activity.class);
        startActivity(intent);
    }

    public void change_password(View view){
        Intent intent = new Intent(this, Change_Password_Activity.class);
        startActivity(intent);
    }

    public void deleteTrans(View view) {
        Intent intent = new Intent(this, DeleteDialogFragment.class);
        startActivity(intent);
    }

    public void saveTrans(View view){
        Intent intent = new Intent(this, Save_Transaction_Activity.class);
        startActivity(intent);
    }
}
