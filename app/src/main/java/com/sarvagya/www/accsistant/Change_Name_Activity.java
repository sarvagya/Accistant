package com.sarvagya.www.accsistant;

import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Change_Name_Activity extends AppCompatActivity {

    private EditText et_new_name;
    private SharedPreferences.Editor editor;
    private TextView tv_current_name, tv_prompt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change__name_);

        tv_current_name = (TextView) findViewById(R.id.tv_current_name);
        tv_prompt = (TextView) findViewById(R.id.tv_prompt);

        tv_current_name.setText(LoginActivity.preferences.getString(LoginActivity.KEY_USERNAME, ""));

        et_new_name = (EditText) findViewById(R.id.et_new_name);

        Button btn_set = (Button) findViewById(R.id.btn_set);
        btn_set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    String new_name = et_new_name.getText().toString();

                    if(new_name.isEmpty()){
                        tv_prompt.setVisibility(View.VISIBLE);
                    }else {

                        editor = LoginActivity.preferences.edit();
                        editor.putString(LoginActivity.KEY_USERNAME, new_name);
                        editor.commit();

                        Toast.makeText(Change_Name_Activity.this, "Account name updated successfully.", Toast.LENGTH_LONG).show();
                        finish();
                    }
            }
        });

    }
}
