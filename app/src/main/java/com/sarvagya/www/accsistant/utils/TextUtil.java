package com.sarvagya.www.accsistant.utils;

import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.widget.TextView;

public class TextUtil {

    public static void typeFace(AssetManager mgr, String fontPath, TextView tv){
        Typeface tf = Typeface.createFromAsset(mgr, fontPath);
        tv.setTypeface(tf);
    }
}
