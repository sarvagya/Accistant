package com.sarvagya.www.accsistant.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sarvagya.www.accsistant.R;
import com.sarvagya.www.accsistant.controller.ExpenseAdapter;
import com.sarvagya.www.accsistant.db.DatabaseHandler;
import com.sarvagya.www.accsistant.models.Expense;

import java.text.DecimalFormat;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class fragment_expense_view extends Fragment {
    private RecyclerView recyclerView;
    private ArrayList<Expense> expenseList;
    private DatabaseHandler handler;
    private TextView tv_totalExpense, tv_highestValue, tv_lowestValue, tv_averageValue, tvNothingText;

    private View view;

    public fragment_expense_view() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_fragment_expense_view, container, false);

        tvNothingText = view.findViewById(R.id.tvNothingText);
        tv_highestValue = view.findViewById(R.id.tv_highestValue);
        tv_totalExpense = view.findViewById(R.id.tv_totalExpense);
        tv_lowestValue = view.findViewById(R.id.tv_lowestValue);
        tv_averageValue = view.findViewById(R.id.tv_averageValue);

        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(false);


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (view != null) {
            handler = new DatabaseHandler(getActivity());
            expenseList = handler.getAllExpense();

            if (expenseList.size() > 0) {
                ExpenseAdapter adapter = new ExpenseAdapter(expenseList, getActivity());
                recyclerView.setAdapter(adapter);
            } else {
                tvNothingText.setVisibility(View.VISIBLE);
            }


            //setting information on the interface
            double total = handler.getTotalExpense();
            tv_totalExpense.setText("TOTAL EXPENSE : " + total);

            tv_highestValue.setText(handler.getHighestValue(2) + "");
            tv_lowestValue.setText(handler.getLowestValue(2) + "");
            tv_averageValue.setText(new DecimalFormat("##.##").format(handler.getAverageValue(2)));
        }
    }
}
