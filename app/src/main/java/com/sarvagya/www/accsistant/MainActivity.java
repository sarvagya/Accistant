package com.sarvagya.www.accsistant;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.transition.Explode;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.sarvagya.www.accsistant.db.DatabaseHandler;

public class MainActivity extends Activity {
    private TextView tv_username, tv_total_income, tv_total_expenses;
    private Boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv_total_income = findViewById(R.id.tv_total_income);
        tv_total_expenses = findViewById(R.id.tv_total_expenses);

        tv_username = findViewById(R.id.tv_username);
        Typeface ralewayExtraLight = Typeface.createFromAsset(getAssets(), "fonts/Raleway-Light.ttf");
        tv_username.setTypeface(ralewayExtraLight);

        get_account_name();

        get_total_income_expenses();


    }

    private void get_total_income_expenses() {
        DatabaseHandler handler = new DatabaseHandler(this);
        double total_income = handler.getTotalIncome();
        double total_expenses = handler.getTotalExpense();

        tv_total_income.setText("Rs " + total_income);
        tv_total_expenses.setText("Rs " + total_expenses);
    }

    @Override
    protected void onResume() {
        super.onResume();

        get_account_name();
        get_total_income_expenses();
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please press the BACK button twice to exit !", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);

    }

    public void postIncome(View view) {
        Intent intent = new Intent(MainActivity.this, Post_Income_Activity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        overridePendingTransition(0,0);
    }

    public void viewAccounts(View view) {
        Intent intent = new Intent(MainActivity.this, View_Accounts_Activity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        overridePendingTransition(0,0);

    }

    public void postExpense(View view) {
        Intent intent = new Intent(MainActivity.this, Post_Expense_Activity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        overridePendingTransition(0,0);
    }

    public void settings(View view) {
        Intent intent = new Intent(MainActivity.this, Settings_Activity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        overridePendingTransition(0,0);
    }

    public void get_account_name() {
        String username = LoginActivity.preferences.getString(LoginActivity.KEY_USERNAME, "");
        if (username != null) {
            username = username.substring(0, 1).toUpperCase() + username.substring(1);
            tv_username.setText(username);
        } else {
            tv_username.setText("Welcome user !");
        }
    }


}
