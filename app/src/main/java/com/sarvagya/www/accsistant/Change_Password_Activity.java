package com.sarvagya.www.accsistant;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import static com.sarvagya.www.accsistant.LoginActivity.*;

public class Change_Password_Activity extends AppCompatActivity {

    private TextView tv1, tv2;
    private EditText et_current_password, et_new_password;
    private Button btn_save;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change__password_);

        tv1 = (TextView) findViewById(R.id.tv1);
        tv2 = (TextView) findViewById(R.id.tv2);
        et_current_password = (EditText) findViewById(R.id.et_current_password);
        et_new_password = (EditText) findViewById(R.id.et_new_password);
        btn_save = (Button) findViewById(R.id.btn_save);

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String current_password = et_current_password.getText().toString();
                String old_password = LoginActivity.preferences.getString(LoginActivity.finalPassword, "");
                String new_password = et_new_password.getText().toString();

                if(current_password.equalsIgnoreCase(old_password)){
                    editor = LoginActivity.preferences.edit();
                    editor.putString(LoginActivity.finalPassword, new_password);

                    Toast.makeText(Change_Password_Activity.this, "Password updated successfully !", Toast.LENGTH_LONG).show();

                    editor.commit();
                    finish();
                }else{
                    Toast.makeText(Change_Password_Activity.this, "Password donot match", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
