package com.sarvagya.www.accsistant;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.mlsdev.animatedrv.AnimatedRecyclerView;
import com.sarvagya.www.accsistant.controller.ExpenseAdapter;
import com.sarvagya.www.accsistant.controller.IncomeAdapter;
import com.sarvagya.www.accsistant.db.DatabaseHandler;
import com.sarvagya.www.accsistant.fragments.DatePickerFragment;
import com.sarvagya.www.accsistant.fragments.DatePickerFragment2;
import com.sarvagya.www.accsistant.models.Expense;
import com.sarvagya.www.accsistant.models.Income;
import com.sarvagya.www.accsistant.utils.RecyclerViewAnimation;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Custom_Search_Activity extends AppCompatActivity implements DatePickerFragment.theDateListener, DatePickerFragment2.dateListener {

    private AppCompatSpinner spinner;
    private Button btn_dateFrom, btn_dateTo;
    private ImageButton btn_search;
    private long date_from, date_to = 0;
    private RecyclerView recyclerView;
    private TextView tv_information, tv_nothing;

    private DatabaseHandler handler;
    private ArrayList<Income> incomeList_main;
    private ArrayList<Expense> expenseList_main;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom__search_);

        tv_information = (TextView) findViewById(R.id.tv_information);
        tv_nothing = (TextView) findViewById(R.id.tv_nothing);

        btn_dateFrom = (Button) findViewById(R.id.btn_dateFrom);
        btn_dateTo = (Button) findViewById(R.id.btn_dateTo);
        btn_search = (ImageButton) findViewById(R.id.btn_search);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(false);

        handler = new DatabaseHandler(this);

        ArrayList<String> list = new ArrayList<>();
        list.add("Income");
        list.add("Expense");

        spinner = (AppCompatSpinner) findViewById(R.id.spinner);
        ArrayAdapter dataAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, list);
        spinner.setAdapter(dataAdapter);
        spinner.setSelection(0);

        btn_dateFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerFragment frag = new DatePickerFragment();
                frag.show(getSupportFragmentManager(), "datepicker");
            }
        });

        btn_dateTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerFragment2 frag2 = new DatePickerFragment2();
                frag2.show(getSupportFragmentManager(), "datepicker2");
            }
        });


        //setting recyclerview with the list.
        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int spinner_pos = spinner.getSelectedItemPosition();

                if (spinner_pos == 0) {
                    incomeList_main = handler.getSortedIncome(date_from, date_to);

                    if (incomeList_main.isEmpty()) {
                        tv_information.setVisibility(View.GONE);
                        tv_nothing.setVisibility(View.VISIBLE);

                    } else {
                        tv_information.setVisibility(View.GONE);
                        tv_nothing.setVisibility(View.GONE);
                        IncomeAdapter adapter = new IncomeAdapter(Custom_Search_Activity.this, incomeList_main);
                        recyclerView.setAdapter(adapter);

                        RecyclerViewAnimation.animationRecyclerView(recyclerView);
                    }


                }

                if (spinner_pos == 1) {
                    expenseList_main = handler.getSortedExpense(date_from, date_to);

                    if (expenseList_main.isEmpty()) {
                        tv_information.setVisibility(View.GONE);
                        tv_nothing.setVisibility(View.VISIBLE);
                    } else {
                        tv_information.setVisibility(View.GONE);
                        tv_nothing.setVisibility(View.GONE);
                        ExpenseAdapter adapter = new ExpenseAdapter(expenseList_main, Custom_Search_Activity.this);
                        recyclerView.setAdapter(adapter);

                        RecyclerViewAnimation.animationRecyclerView(recyclerView);

                    }


                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_custom_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.refresh) {

            finish();
            overridePendingTransition(0, 0);
            startActivity(getIntent());
            overridePendingTransition(0, 0);
        }

        return true;
    }

    @Override
    public void onClick(long unixDate) {

        date_from = unixDate * 1000L;
        Date date = new Date(date_from);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String formatted_date_from = sdf.format(date);

        btn_dateFrom.setText(formatted_date_from);
    }

    @Override
    public void onDateTransferred(long unixDate) {

        date_to = unixDate * 1000L;
        Date date = new Date(date_to);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String formatted_date_to = sdf.format(date);

        btn_dateTo.setText(formatted_date_to);
    }

}
