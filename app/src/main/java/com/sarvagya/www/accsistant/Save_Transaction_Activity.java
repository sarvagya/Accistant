package com.sarvagya.www.accsistant;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.microsoft.onedrivesdk.saver.ISaver;
import com.microsoft.onedrivesdk.saver.Saver;
import com.microsoft.onedrivesdk.saver.SaverException;
import com.sarvagya.www.accsistant.db.DatabaseHandler;
import com.sarvagya.www.accsistant.models.Expense;
import com.sarvagya.www.accsistant.models.Income;

import org.w3c.dom.Text;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Save_Transaction_Activity extends AppCompatActivity {

    public static final int REQUEST_CODE = 301;

    private TextView tv_month;
    private String ONEDRIVE_Client_ID = "5b4cc31c-6a29-42ab-9c5b-cf888a566222";
    private ISaver mSaver;
    private String filename;
    private TextView tvFileName;
    private RadioButton rIncome, rExpense;
    private Button btnOneDrive;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save__transaction_);

        btnOneDrive = (Button) findViewById(R.id.btnOneDrive);
        rIncome = (RadioButton) findViewById(R.id.rIncome);
        rExpense = (RadioButton) findViewById(R.id.rExpense);

        tvFileName =(TextView) findViewById(R.id.tvFileName);

        filename = "backup" + System.currentTimeMillis() / 1000L + ".csv";
        tvFileName.setText(filename);

        btnOneDrive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String oneDriveFile = filename;  //pass the filename here

                //file path
                File file = new File(Environment.getExternalStorageDirectory() + "/Documents" + "/Accsistant", oneDriveFile); //might get a problem here with the context.

                //create and launch the saver
                mSaver = Saver.createSaver(ONEDRIVE_Client_ID);
                mSaver.startSaving((Activity) view.getContext(), oneDriveFile, Uri.fromFile(file));

            }
        });
    }

    public void saveToDatabase(View view) {
        askPermission();

    }

    public void writeFile() {

        DatabaseHandler handler = new DatabaseHandler(this);
        ArrayList<Income> incomeList = handler.getAllIncome();
        ArrayList<Expense> expenseList = handler.getAllExpense();

        try {
            File directory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS) + "/Accsistant");
            directory.mkdir();

            File file = new File(directory, filename);
            FileOutputStream outputStream = new FileOutputStream(file);

            StringBuilder sb = new StringBuilder();
            sb.append("Date,Income Amount,Income Details" + "\n");

            if (rIncome.isChecked()){
                for (Income income : incomeList) {
                    long datebaseDate = income.getDateTime();
                    java.util.Date date = new java.util.Date(datebaseDate);
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    String formattedDate = sdf.format(date);
                    sb.append(formattedDate + "," + income.getIncomeAmt() + "," + income.getIncomeDetails() + "\n");
                }
            }else{
                for (Expense expense : expenseList) {
                    long databaseDate2 = expense.getDate();
                    java.util.Date date2 = new java.util.Date(databaseDate2);
                    SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
                    String formattedDate2 = sdf2.format(date2);
                    sb.append(formattedDate2 + "," + expense.getExpenseAmt() + "," + expense.getExpenseDetails() + "\n");
                }
            }
            outputStream.write(sb.toString().getBytes());
            outputStream.close();

            Snackbar bar = Snackbar.make(rIncome, "File has been created to your 'Documents' folder", Snackbar.LENGTH_LONG);
            bar.show();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        //check that the file was successfully saved to OneDrive
        try {
            mSaver.handleSave(requestCode, resultCode, data);
        } catch (final SaverException e) {
            //Log error information
            //SaverError type
            Log.e("OneDriveSaver", e.getErrorType().toString());
        }

        // Handle non-OneDrive picker request
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void askPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                writeFile();

            } else {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE);
            }
        } else {
            writeFile();

        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                writeFile();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
