package com.sarvagya.www.accsistant;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sarvagya.www.accsistant.utils.TextUtil;

public class LoginActivity extends Activity {

    private Button btn_login;
    private EditText et_username, et_password;

    private TextInputLayout layout1;

    public static SharedPreferences preferences;
    private static final String PREF_NAME = "test_pref";
    public static final String KEY_USERNAME = "name";
    public static final String finalPassword = null;
    private String username, password;
    private SharedPreferences.Editor editor;

    private TextView tv_show_name, tv_login, tv_appName;

    private Boolean firstRun;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btn_login = (Button) findViewById(R.id.btn_login);
        et_username = (EditText) findViewById(R.id.et_username);
        et_password = (EditText) findViewById(R.id.et_password);

        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);

        tv_show_name = findViewById(R.id.tv_show_name);
        tv_appName = findViewById(R.id.tv_appName);
        TextUtil.typeFace(getAssets(), "fonts/Philosopher-Regular.ttf", tv_appName);

        tv_login = findViewById(R.id.tv_login);
        TextUtil.typeFace(getAssets(), "fonts/Raleway-Light.ttf", tv_login);


        if (preferences.getBoolean("firstRun", true)){

        }else{
            editor = preferences.edit();

            et_username.setVisibility(View.GONE);

            tv_show_name.setVisibility(View.VISIBLE);
            String saved_username = preferences.getString(KEY_USERNAME, "");
            tv_show_name.setText(saved_username);
            TextUtil.typeFace(getAssets(), "fonts/Raleway-Light.ttf", tv_show_name);

        }
    }


    public void btn_login(View view) {
        final AlphaAnimation buttonClick = new AlphaAnimation(0.2F, 0.3F);
        view.startAnimation(buttonClick);

        username = et_username.getText().toString();
        password = et_password.getText().toString();

        if(username.isEmpty() && tv_show_name == null){
            Toast.makeText(LoginActivity.this, "Please the account name", Toast.LENGTH_SHORT).show();
        }else {

            if (preferences.getBoolean("firstRun", true)){

                editor = preferences.edit();
                editor.putString(KEY_USERNAME, username);
                editor.putString(finalPassword, password);
                editor.putBoolean("firstRun", false);
                editor.commit();

                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                finish();

            } else if(password.equalsIgnoreCase(preferences.getString(finalPassword, ""))){
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                finish();
            }else {
                Toast.makeText(this, "Wrong password", Toast.LENGTH_LONG).show();
            }


        }




    }
}
