package com.sarvagya.www.accsistant.models;

/**
 * Created by User on 10/16/2017.
 */

public class Income {

    private int income_id;
    private double incomeAmt;
    private String incomeDetails;
    private long dateTime;
    private String moreDetails;

    public Income() {

    }

    public Income(double incomeAmt, String incomeDetails, long dateTime, String moreDetails) {
        this.incomeAmt = incomeAmt;
        this.incomeDetails = incomeDetails;
        this.dateTime = dateTime;
        this.moreDetails = moreDetails;
    }

    public int getIncome_id() {
        return income_id;
    }

    public void setIncome_id(int income_id) {
        this.income_id = income_id;
    }

    public double getIncomeAmt() {
        return incomeAmt;
    }

    public void setIncomeAmt(double incomeAmt) {
        this.incomeAmt = incomeAmt;
    }

    public String getIncomeDetails() {
        return incomeDetails;
    }

    public void setIncomeDetails(String incomeDetails) {
        this.incomeDetails = incomeDetails;
    }

    public long getDateTime() {
        return dateTime;
    }

    public void setDateTime(long dateTime) {
        this.dateTime = dateTime;
    }

    public String getMoreDetails() {
        return moreDetails;
    }

    public void setMoreDetails(String moreDetails) {
        this.moreDetails = moreDetails;
    }


}
